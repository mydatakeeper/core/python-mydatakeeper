import logging

logger = logging.getLogger("fr.mydatakeeper.application")

class UnitHandler:
    properties_iface = 'org.freedesktop.DBus.Properties'

    systemd_name = 'org.freedesktop.systemd1'
    systemd_path = '/org/freedesktop/systemd1'
    systemd_manager_iface = 'org.freedesktop.systemd1.Manager'
    systemd_unit_iface = 'org.freedesktop.systemd1.Unit'

    def __init__(self, bus, unit_name):
        logger.debug("Getting systemd dbus proxy")
        systemd_proxy = bus.get(self.systemd_name, self.systemd_path)
        logger.debug("Loading unit")
        unit_path = systemd_proxy[self.systemd_manager_iface].LoadUnit(unit_name)
        logger.debug("Getting unit dbus proxy")
        unit_proxy = bus.get(self.systemd_name, unit_path)
        logger.debug("Connecting to PropertiesChanged signal")
        unit_proxy[self.properties_iface].PropertiesChanged.connect(self.unitChanged)
        self.unit = unit_proxy[self.systemd_unit_iface]

    def setup(self):
        self.reloadOrRestart()

    def teardown(self):
        self.stop()

    def start(self, mode='replace'):
        logger.debug("Starting unit")
        self.unit.Start(mode)

    def stop(self, mode='replace'):
        logger.debug("Stopping unit")
        self.unit.Stop(mode)

    def restart(self, mode='replace'):
        logger.debug("Restarting unit")
        self.unit.Restart(mode)

    def reload(self, mode='replace'):
        logger.debug("Reloading unit")
        self.unit.Reload(mode)

    def reloadOrRestart(self, mode='replace'):
        logger.debug("Reloading or restarting unit")
        self.unit.ReloadOrRestart(mode)

    def unitChanged(self, interface_name, changed_properties, invalidated_properties):
        pass

class ConfigHandler(UnitHandler):
    properties_iface = 'org.freedesktop.DBus.Properties'
    mydatakeeper_config_iface = 'fr.mydatakeeper.Config'

    def __init__(self, bus, bus_name, bus_path):
        logger.debug("Getting config Dbus proxy")
        config_proxy = bus.get(bus_name, bus_path)
        logger.debug("Connecting to PropertiesChanged signal")
        config_proxy[self.properties_iface].PropertiesChanged.connect(self.configChanged)
        self.config = config_proxy[self.mydatakeeper_config_iface]

    def setup(self):
        self.generateConfig()

    def teardown(self):
        self.deleteConfig()

    def configChanged(self, interface_name, changed_properties, invalidated_properties):
        pass

    def generateConfig(self):
        pass

    def deleteConfig(self):
        pass

class UnitConfigHandler(ConfigHandler,UnitHandler):
    def __init__(self, bus, bus_name, bus_path, unit_name):
        ConfigHandler.__init__(self, bus, bus_name, bus_path)
        UnitHandler.__init__(self, bus, unit_name)

    def setup(self):
        ConfigHandler.setup(self)
        UnitHandler.setup(self)

    def teardown(self):
        UnitHandler.teardown(self)
        ConfigHandler.teardown(self)

    def configChanged(self, interface_name, changed_properties, invalidated_properties):
        if interface_name != ConfigHandler.mydatakeeper_config_iface:
            return
        if ConfigHandler.generateConfig(self):
            UnitHandler.reloadOrRestart(self)
