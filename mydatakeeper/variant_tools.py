from gi.repository.GLib import Variant

def to_variant(value):
    sig = get_signature(value)
    val = apply_signature(sig,value)
    return Variant(sig, val)

def get_signature(value):
    if value is None:
        raise TypeError("None is not a valid dbus value")
    elif type(value) is bool:
        return 'b'
    elif type(value) is int:
        return 'u'
    elif type(value) is float:
        return 'd'
    elif type(value) is str:
        return 's'
    elif type(value) is Variant:
        return 'v'
    elif type(value) is tuple:
        if len(value) == 0:
            return '()'
        return '({})'.format(''.join(map(
            lambda v: get_signature(v),
            value
        )))
    elif type(value) is list:
        if len(value) == 0:
            return 'as'
        types = set(map(lambda v: get_signature(v), value))
        if len(types) == 1:
            return 'a{}'.format(types.pop())
        return 'av'
    elif type(value) is dict:
        if len(value) == 0:
            return 'a{ss}'
        key_types = set(map(lambda v: get_signature(v), value.keys()))
        value_types = set(map(lambda v: get_signature(v), value.values()))
        if len(key_types) != 1:
            raise TypeError("Dictionnary keys are not of the same type", value)
        elif len(value_types) == 1:
            return 'a{{{}{}}}'.format(key_types.pop(), value_types.pop())
        else:
            return 'a{{{}v}}'.format(key_types.pop())
    else:
        raise TypeError("Unknown DBus type", value)

def apply_signature(sig,value):
    n, result = _apply_signature(sig, value)
    if n != len(sig):
        raise TypeError("Invalid dbus signature", sig)
    return result

def _apply_signature(sig, value):
    if sig == '':
        raise TypeError("Empty signature type")
    elif sig[0] == 'b':
        return (1, bool(value))
    elif sig[0] == 'u':
        return (1, int(value))
    elif sig[0] == 'd':
        return (1, float(value))
    elif sig[0] == 's':
        return (1, str(value))
    elif sig[0] == 'v':
        sig1 = get_signature(value)
        return (1, Variant(sig1, apply_signature(sig1,value)))
    elif sig[0] == '(':
        sig_len = 1
        idx = 0
        tpl = ()
        while sig[sig_len] != ')':
            n, res = _apply_signature(sig[sig_len:],value[idx])
            sig_len += n
            idx += 1
            tpl += res
        return (sig_len+1, tpl)
    elif sig[0] == 'a' and sig[1] != '{':
        if len(value) == 0:
            return (len(sig), value)
        sig_lens = set()
        arr = list()
        for v in value:
            n, res = _apply_signature(sig[1:],v)
            sig_lens.add(n)
            arr.append(res)
        if len(sig_lens) != 1:
            raise TypeError("Invalid dbus signature", sig)
        return (sig_lens.pop()+1,arr)
    elif sig[0] == 'a' and sig[1] == '{':
        if len(value) == 0:
            return (len(sig), value)
        sig_lens1 = set()
        sig_lens2 = set()
        dic = dict()
        for k, v in value.items():
            n1, res1 = _apply_signature(sig[2:],k)
            n2, res2 = _apply_signature(sig[2+n1:],v)
            sig_lens1.add(n1)
            sig_lens2.add(n2)
            dic[res1] = res2
        if len(sig_lens1) != 1:
            raise TypeError("Invalid dbus signature", sig)
        if len(sig_lens2) != 1:
            raise TypeError("Invalid dbus signature", sig)
        sig_len1 = sig_lens1.pop()
        sig_len2 = sig_lens2.pop()
        if sig[2+sig_len1+sig_len2] != '}':
            raise TypeError("Invalid dbus signature", sig)
        return (2+sig_len1+sig_len2+1,dic)
    else:
        raise TypeError("Unknown DBus type", value)
