import getopt
import sys

class Parser:
    def __init__(self, arguments, version):
        if 'help' not in arguments:
            arguments['help'] = {
                'longopt': 'help',
                'shortopt': 'h',
                'description': "Display this help message and exit",
                'callback': lambda o, a: self.show_usage(0),
            }
        if 'version' not in arguments:
            arguments['version'] = {
                'longopt': 'version',
                'shortopt': 'v',
                'description': "Display the current version and exit",
                'callback': lambda o, a: self.show_version(),
            }
        self.arguments = arguments
        self.version = version

    def show_version(self):
        print(sys.argv[0])
        print("\tversion: {}".format(self.version))
        sys.exit()

    def show_usage(self, err):
        print("usage: ", sys.argv[0], " [options ...]")
        print()
        print("options:")

        maxlen = max(map(lambda arg:
            len(arg.get('shortopt', '')) + len(arg.get('longopt', '')) + 8,
            self.arguments.values()))
        for arg in self.arguments.values():
            longopt = ('--' + arg['longopt']) if 'longopt' in arg else ''
            shortopt = ('-' + arg['shortopt']) if 'shortopt' in arg else ''
            opt = '  ' + shortopt + (',' if longopt != '' and shortopt != '' else '') + longopt
            print(opt.ljust(maxlen), arg.get('description'))
        sys.exit(err)

    def run(self):
        try:
            opts, args = getopt.getopt(
                sys.argv[1:],
                ''.join(map(lambda arg:
                    arg['shortopt'] + (':' if arg.get('required') else ''),
                    filter(lambda arg: 'shortopt' in arg, self.arguments.values()))),
                list(map(lambda arg:
                    arg['longopt'] + ('=' if arg.get('required') else ''),
                    filter(lambda arg: 'longopt' in arg, self.arguments.values()))),
            )
        except getopt.GetoptError as err:
            print(err)
            self.show_usage(2)

        for name, value in opts:
            found = False
            for arg in self.arguments.values():
                if name == '-' + arg.get('shortopt', '') or name == '--' + arg.get('longopt', ''):
                    if not arg.get('required'):
                        value = True
                    arg['value'] = value

                    found = True
            if not found:
                assert False, "Unkown argument '{}'".format(o)

    def value(self, name):
        arg = self.arguments[name]
        return self._callback(arg, name, arg.get('value', arg.get('default', None)))

    def _callback(self, arg, name, value):
        if 'callback' in arg:
            value = arg['callback'](name, value)
            if value is None:
                print("Invalid value for argument '{}'".format(name))
                self.show_usage(2)
        return value
